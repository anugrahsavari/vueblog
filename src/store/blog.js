import axios from 'axios';

export default {
	namespaced: true,
	state: {
		title: '',
		description: '',
		blog: {},
		blogs: [],
		isUpload: false,
		statusForm: 'submit',
		paginateBlogs: {},
		page: 0,
		pageLength: 0,
		perPage: 0,
	},
	mutations: {
		setBlogs: (state, blogs) => {
			state.blogs = blogs;
		},
		setBlog: (state, blog) => {
			state.blog = blog;
		},
		setBlogTitle: (state, title) => {
			state.title = title;
		},
		setBlogDescription: (state, desc) => {
			state.description = desc;
		},
		setStatusUpload: (state, value) => {
			state.isUpload = value;
		},
		setStatusForm: (state, value) => {
			state.statusForm = value;
		},
		setPaginateBlogs: (state, value) => {
			state.paginateBlogs = value;
		},
		setPage: (state, value) => {
			state.page = value;
		},
		setPageLength: (state, value) => {
			state.pageLength = value;
		},
		setPerPage: (state, value) => {
			state.perPage = value;
		},
	},
	actions: {
		getBlogs: ({ commit }) => {
			axios
				.get('https://demo-api-vue.sanbercloud.com/api/v2/blog/random/4')
				.then((response) => {
					let { blogs } = response.data;
					commit('setBlogs', blogs);
				})
				.catch((error) => {
					console.log(error);
				});
		},
		getPaginateBlogs: ({ commit, state }) => {
			axios
				.get(
					'https://demo-api-vue.sanbercloud.com/api/v2/blog?page=' + state.page
				)
				.then((response) => {
					let { blogs } = response.data;
					commit('setPaginateBlogs', blogs);
					commit('setBlogs', blogs.data);
					commit('setPage', blogs.current_page);
					commit('setPageLength', blogs.last_page);
					commit('setPerPage', blogs.per_page);
				})
				.catch((error) => {
					console.log(error);
				});
		},
		setPage: ({ commit }, val) => {
			commit('setPage', val);
		},
		setPageLength: ({ commit }, val) => {
			commit('setPageLength', val);
		},
		setPerPage: ({ commit }, val) => {
			commit('setPerPage', val);
		},
		setBlogs: ({ commit }, blogs) => {
			commit('setBlogs', blogs);
		},
		setBlog: ({ commit }, blog) => {
			commit('setBlog', blog);
			commit('setBlogTitle', blog?.title || '');
			commit('setBlogDescription', blog?.description || '');
		},
		setBlogTitle: ({ commit }, title) => {
			commit('setBlogTitle', title);
		},
		setBlogDescription: ({ commit }, desc) => {
			commit('setBlogDescription', desc);
		},
		setStatusUpload: ({ commit }, value) => {
			commit('setStatusUpload', value);
		},
		setStatusForm: ({ commit }, value) => {
			commit('setStatusForm', value);
		},
	},
	getters: {
		blogs: (state) => state.blogs,
		blogItem: (state) => state.blog,
		paginateBlogs: (state) => state.paginateBlogs,
		page: (state) => state.page,
		pageLength: (state) => state.pageLength,
		perPage: (state) => state.perPage,
		title: (state) => state.title,
		description: (state) => state.description,
		isUpload: (state) => state.isUpload,
		statusForm: (state) => state.statusForm,
	},
};
