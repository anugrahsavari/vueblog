import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import alert from './alert';
import auth from './auth';
import blog from './blog';
import dialog from './dialog';

const vuexPersist = new VuexPersist({
	key: 'airflow',
	storage: localStorage,
});

Vue.use(Vuex);

export default new Vuex.Store({
	plugins: [vuexPersist.plugin],
	modules: {
		alert,
		dialog,
		auth,
		blog,
	},
});
